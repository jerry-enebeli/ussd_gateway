const express = require('express');
const grpc = require('grpc');
const bodyParser = require('body-parser');
const Joi = require('joi');

const PROTO_PATH = `${__dirname}/proto/auth.proto`;

const AuthService = grpc.load(PROTO_PATH).eyowo_auth;
const AuthRPC = new AuthService.Auth('0.0.0.0:50051', grpc.credentials.createInsecure());

const router = express.Router();


// TEST ROUTE
router.get('/', (req, res) => {
  AuthRPC.testMessage({ mobile: '2348066953623' }, (err, response) => {
    if (err) {
      console.log(err);
    }
    console.log(response);
    res.json(response);
  });
});


router.post('/validate/mobile', bodyParser.json(), (req, res) => {
  console.log(req.body);

  // Validate request
  const schemaValidate = Joi.object().keys({
    mobile: Joi.string().regex(/^[0-9]{9,13}$/).required(),
  });

  const validateResult = Joi.validate(req.body, schemaValidate);

  if (validateResult.error !== null) {
    return res.json({
      success: false,
      message: validateResult.error.details[0].message,
    });
  }

  try {
    AuthRPC.validateMobile({ mobile: req.body.mobile }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
});

router.post('/validate/securepin', bodyParser.json(), async (req, res) => {
  const schemaValidate = Joi.object().keys({
    mobile: Joi.string().regex(/^[0-9]{9,13}$/).required(),
    secure_pin: Joi.string().regex(/^[0-9]{6}$/).required(),
  });

  const validateResult = Joi.validate(req.body, schemaValidate);

  if (validateResult.error !== null) {
    return res.json({
      success: false,
      message: validateResult.error.details[0].message,
    });
  }

  try {
    AuthRPC.validateSecurePIN({ mobile: req.body.mobile, secure_pin: req.body.secure_pin }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      if (response.success) {
        response.user = JSON.parse(response.user);
      }
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
});

module.exports = router;
