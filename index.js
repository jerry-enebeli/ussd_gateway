const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const AirbrakeClient = require('airbrake-js');
require('dotenv').config();

const app = express();

const auth = require('./app/routes/v1/auth');
const users = require('./app/routes/v1/users');

// Enable HTTP request logging
app.use(morgan('combined'));
// Enable CORS
app.use(cors());
// CORS pre-flight
app.options('*', cors());
// Extract IP Info
app.use((req, res, next) => {
  req.remote_ip_address = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  next();
});

app.get('/', (req, res) => {
  res.send('Welcome to the Eyowo USSD Service!');
});

app.use('/v1/auth', auth);
app.use('/v1/users', users);

const server = app.listen(process.env.SERVER_PORT || 4000, () => {
  const host = server.address().address;
  const { port } = server.address();
  process.on('uncaughtException', (err) => {
    console.error(err, 'Uncaught Exception thrown');
    process.exit(1);
  });
  console.log('Eyowo API Service listening at http://%s:%s', host, port);
});


const airbrake = new AirbrakeClient({
  projectId: 184436,
  projectKey: '4f5138863964f2bc7a03515d3d773ba6',
});
